mimenator
=========

Rails plugin for defining vendor/personal/unregistered media types.

Features
--------

* Register vendor, personal, unregistered media types with Rails
* Maps your type to a known media format (currently only JSON)

Usage
-----

### Define a type ###

Define in an initializer, say `config/initializers/mimenator.rb`.

The following will result in a MIME type `application/vnd.bobo+json; version=1`

```ruby
Mimenator.register(:bobo_v1_json) do |type|
  type.producer = 'bobo'
  type.version = 1
  type.suffix = :json
end
```

If you want to embed the version in your type, you'd simply change the above to:

```ruby
Mimenator.register(:bobo_v1_json) do |type|
  type.producer = 'bobo'
  type.version = 1
  type.version_style = :nested
  type.suffix = :json
end
```

...which would end up giving you `application/vnd/bobo.v1+json`.  See the docs
for `Mimenator::Type` for more info.

### Use the type ###

Now that you've defined a type, `bobo_v1_json`, you can tell your Rails things
to use it.

#### Models ####

Use the newly defined method:

```ruby
# app/models/thingy.rb
class Thingy < ActiveRecord::Base
end

# test.rb
thingy = Thingy.new
thingy.to_bobo_v1_json    # => '{"disposition":null}'
thing.disposition = :happy
thingy.to_bobo_v1_json    # => '{"disposition":"happy"}'
```

#### Controllers ####

First, include +Mimenator::ControllerMixin+ to make sure your format type gets
set properly from the Accept header.  Then, tell your controller to `respond_to`
the new media type:

```ruby
class ApplicationController < ActionController::Base
  include Mimenator::ControllerMixin
end

class ThingiesController < ApplicationController
  respond_to :bobo_v1_json

  def show
    @thing = Thingy.find(params[:id])
    respond_with @thing
  end
end
```

The thing here (no pun intended), is defining the payload for your `@thing` in
`#show`.  Because Mimenator defines `bobo_v1_json` to serialize using
`#to_json`, if you don't tell it any special way to format your `@thing`, it'll
just serialize using the normal `#to_json`, which may not be what you're wanting
to do (assuming, since you're here).  If that's all you want, you're all set.
If you do want to differentiate your custom format `@thing`, then it's on you
to serialize in whatever manner you choose:

* **active_model_serializers**: Will behave normally.  Remember though, that if
  you're using something like `respond_to :json, :bobo_v1_json`, AMS will
  default to use `ThingySerializer` for both, so you'd need to define another
  serializer in order to differentiate between the two (if, of course, that's
  what you're after).
* **jbuilder**, **rabl**, etc: Name files in the `views` directly using the media
  type: `app/views/show.bobo_v1_json.json`.
* `Thingy#as_json`: There are a few ways you could go about this approach; one
  might be to handle an optional param that dictates which format to use...

#### Making Requests ####

Assuming my `config/routes.rb` looks like:

```ruby
MyApp::Application.routes.draw do
  get '/thingies/:id', to: 'thingies#show', as: 'thingy'
end
```

I can now do ask for the custom type in a couple ways:

1. Using the URI: `GET /thingies/123.bobo_v1_json`
+ Using an `Accept` header:
  ```
  GET /thingies/123 HTTP/1.1
  Accept: application/vnd.bobo+json; version=1
  ```

Testing
-------

### Controllers ###

Making requests here is just like you normally would:

```ruby
test 'it looks like JSON' do
  get thingy_path(123)
  assert_match 'application/vnd.bobo+json; version=1', headers['Content-Type']
  assert_nothing_raised { JSON.parse(response.body) }
end
```

Installation
------------

Add this line to your application's Gemfile:

    gem 'mimenator'

And then execute:

    $ bundle

Contributing
------------

1. Fork it
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create new Pull Request

License
-------

This project rocks and uses MIT-LICENSE.
