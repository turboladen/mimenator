require 'active_record/base'

# Using the .inherited hook to define methods on the inheriter for being able to
# call #to_[rails format] on them.  This is required in order to be able to
# serialize the inheriter to the custom type; it does so by mapping the
# #to_[rails format] method to the #to_[suffix] method, where the #to_[suffix]
# method should already be defined.
#
# For example, if I define a rails_format +:neat_v1_json+, where the suffix is
# +:json+, this will define a +#to_neat_v1_json+ method that, when called, will
# call +#to_json+.
class ActiveRecord::Base
  def self.inherited(subclass)
    super(subclass)

    Mimenator.types.each do |type|
      def_method_name = "to_#{type.rails_format}".to_sym

      subclass.send(:define_method, def_method_name) do |options={}|
        call_method_name = "to_#{type.suffix}".to_sym

        self.send(call_method_name, options)
      end
    end
  end
end
