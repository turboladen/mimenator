class Mimenator
  # This must be required in the controllers you wish to handle your custom
  # MIME types.  You can, of course, include it in ApplicationController if you
  # wish to let all controllers handle custom MIME types (and why wouldn't
  # you!?).
  module ControllerMixin
    class << self

      # Adds a +before_action+ to the includer that sets the request.format
      # according to the Accept header.  This may be a hack, but I can't tell...
      # For the routes that I've tested on (all JSON routes), even though I send
      # my custom MIME type in the +Accept+ header and Rails sees it, Rails never
      # sets the +respond_to+ format to match the custom MIME type--it just
      # remains set to JSON and the custom handler never gets called.  This
      # makes sure the format gets set.
      #
      # For more info on how the format gets set, see
      # actionpack/lib/action_dispatch/http/mime_negotiation.rb.
      #
      # @param base [Class]
      # @see ActionDispatch::Http::MimeNegotiation
      def included(base)
        base.before_action do
          type = Mimenator.types.find do |type|
            request.accepts.include? Mime::Type.lookup(type.name)
          end

          if type && request.format != type.try(:rails_format)
            request.format = type.rails_format
          end
        end
      end
    end
  end
end
