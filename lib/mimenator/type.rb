class Mimenator
  # Allows for defining a new media type.
  class Type
    # @return [Symbol] The identifer used for respond_to/respond_with.  I.e.
    #   format.my_mime_type { #... }.  Also used to define the #to_ method on
    #   models.
    attr_accessor :rails_format

    # @return [String] The string used in
    #   "application/vnd.[subtype]+json"
    attr_accessor :subtype

    # @return [String] The string used in
    #   "application/vnd.neat.things.v1+[suffix]"
    attr_accessor :suffix

    # @return [String] The string used in
    #   "application/[tree].neat.things.v1+json"
    attr_accessor :tree

    # @return [Fixnum]
    attr_accessor :version

    # @return [Symbol] Determines the style of versioning to use.  +:trailing+
    #   will append the version after the media type, like:
    #   application/vnd.neat.things+json; version=1
    #   +:nested+ will include the version in the media type, like:
    #   application/vnd.neat.thing.v1+json
    attr_accessor :version_style

    # @param tree [String, Symbol]
    # @param suffix [String, Symbol]
    def initialize(tree, suffix)
      @subtype = nil
      @suffix = suffix
      @tree = tree
      @version = nil
      @version_style = :trailing
    end

    # @return [String] The MIME type string that would be used in an +Accept+ or
    #   +Content-Type+ header.
    def to_s
      @version_style == :trailing ? "#{name}; version=#{@version}" : name
    end

    # The name part of the MIME type (aka, the part before the ; if parameters
    # are given).
    #
    # @return [String]
    # @raise [RuntimeError] if @subtype isn't set.
    def name
      fail 'subtype not set' if @subtype.nil?

      @name ||= case @version_style
      when :trailing then trailing_string_name
      when :nested then nested_string_name
      else
        fail "Unknown version style: #{@version_style}.  Please choose :trailing or :nested"
      end
    end

    private

    def trailing_string_name
      "application/#{@tree}.#{@subtype}+#{@suffix}"
    end

    def nested_string_name
      "application/#{@tree}.#{@subtype}.#{@version}+#{@suffix}"
    end
  end
end
