require 'action_dispatch/http/mime_type'

module Mime
  class Type
    class << self
      alias_method :old_register, :register

      def register(string, symbol, mime_type_synonyms = [], extension_synonyms = [], skip_lookup = false, **parameters)
        old_register(string, symbol, mime_type_synonyms, extension_synonyms, skip_lookup)

        type = lookup(string)
        type.parameters = parameters if type
      end
    end

    attr_accessor :parameters

    def parameters
      @parameters ||= {}
    end
  end
end
