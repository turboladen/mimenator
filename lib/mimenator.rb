require_relative 'mimenator/type'
require_relative 'action_dispatch/http/mime_type_ext'

class Mimenator
  # Used to keep track of the formats that have been registered with Mimenator.
  #
  # @return [Array<Hash{rails_format => String, suffix => String}]
  def self.types
    @types ||= []
  end

  # Register the media type with Rails.  Parameters here are used to build the
  # string that becomes the MIME type:
  #
  #   "application/[tree].[subtype]+[suffix]"
  #
  # @param rails_format [Symbol] The symbol for which you wish to refer to the
  #   format/respond_to type in Rails.  For example, if you pass in +my_type+,
  #   then in your Rails controller, you can say `respond_to :json, :my_type`.
  #   A suggested best practice would be to name this to include information
  #   about the subtype and the suffix; if you're including versioning, include
  #   that as well.  For example, if I work for a company "Neat Stuff", am
  #   registering a type that follows JSON, and am at version 1.1, then I might
  #   call the type +:neat_v1_1_json+.  This is entirely up to you though.
  #
  # @param tree [String, Symbol] Prefix to the mime type.  You can pass in anything
  #   here, but you should probably follow the rules as defined in the RFC.
  #   For "vendor" media, types, you'd pass in +:vnd+, which will, in turn, get
  #   used to build +"application/vnd.rails_format+json"+.
  #
  # @param suffix [String, Symbol] The payload format.  See more here:
  #   http://en.wikipedia.org/wiki/Internet_media_type#Suffix.
  #
  # @yields [Mimenator::Type]
  #
  def self.register(rails_format, tree: :vnd, suffix: :json, &block)
    type = Mimenator::Type.new(tree, suffix)
    block.call(type)
    type.rails_format = rails_format

    Rails.logger.debug "<Mimenator> Registered type: #{type}"
    Mime::Type.register(type.name, rails_format, [], [], false, version: type.version)
    self.types << type

    case suffix
    when :json
      add_json_type(type)
    end
  end

  # Adds the type to the list of ActionController::Renderers that Rails knows
  # about.
  #
  # @private
  def self.add_json_type(type)
    mime_type = Mime::Type.lookup(type.to_s)

    ActionController::Renderers.add(type.rails_format) do |object, _|
      self.content_type ||= mime_type
      self.response_body = object.send("to_#{type.rails_format}")
    end

    ActionDispatch::ParamsParser::DEFAULT_PARSERS[mime_type] = -> (body) {
      ActiveSupport::JSON.decode(body)
    }
  end
  private_class_method :add_json_type
end

# This needs to be required *after* +Mimenator+ has been defined.
require_relative 'active_record/base_ext'
