require 'test_helper'

class BoboV1JsonDeliveryTest < ActionDispatch::IntegrationTest
  test 'bobo_v1_json request returns JSON' do
    get home_path(format: :bobo_v1_json)

    expected_body = { things: [] }.to_json
    assert_equal expected_body, response.body
    assert_match 'application/vnd.bobo+json', headers['Content-Type']
  end

  test 'respond_with and respond_to behave normally with custom type at #index' do
    things :thing1
    things :thing2

    get things_path(format: :bobo_v1_json)

    expected_body = {
      'things' => [
        things(:thing1).to_json,
        things(:thing2).to_json
      ]
    }

    assert_equal expected_body, JSON(response.body)
    assert_match 'application/vnd.bobo+json', headers['Content-Type']
  end

  test 'respond_with and respond_to behave normally with custom type at #show' do
    thing1 = things :thing1

    get thing_path(thing1, format: :bobo_v1_json)

    assert_equal thing1.to_json, response.body
    assert_match 'application/vnd.bobo+json', headers['Content-Type']
  end

  test 'respond_with and respond_to behave normally with custom type at #create' do
    new_thing = { name: 'A new thing', age: 99 }

    assert_difference('Thing.count') do
      post things_path(thing: new_thing, format: :bobo_v1_json)
      assert_response 201
      parsed_body = JSON.parse(response.body, symbolize_names: true)
      assert_equal new_thing[:name], parsed_body[:name]
      assert_equal new_thing[:age], parsed_body[:age]
      assert_match 'application/vnd.bobo+json', headers['Content-Type']
    end
  end

  test 'it does not respond to normal application/json' do
    assert_raise(ActionController::UnknownFormat) { get things_path(format: :json) }
  end
end
