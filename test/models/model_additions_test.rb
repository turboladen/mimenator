require 'test_helper'

class ThingTest < ActiveSupport::TestCase
  def setup
    @model = things(:thing1)
  end

  test 'registering a MIME type adds #to_[rails_format] method to models' do
    assert_respond_to @model, :to_bobo_v1_json
  end

  test '#to_bobo_v1_json returns JSON' do
    converted_json = {
      id: @model.id,
      name: 'Thing 1',
      age: 42,
      created_at: @model.created_at,
      updated_at: @model.updated_at
    }.to_json

    assert_equal converted_json, @model.to_bobo_v1_json
    assert_nothing_raised { JSON.parse(@model.to_bobo_v1_json) }
  end
end
