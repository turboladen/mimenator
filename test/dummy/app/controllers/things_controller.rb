class ThingsController < ApplicationController
  respond_to :bobo_v1_json

  def index
    @things = Thing.all.order(id: :desc)
    respond_with @things
  end

  def show
    @thing = Thing.find(params[:id])
    respond_with @thing
  end

  def create
    @thing = Thing.create(things_params)
    respond_with @thing
  end

  private

  def things_params
    params.require(:thing).permit(:name, :age)
  end
end
