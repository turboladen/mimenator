class HomeController < ApplicationController
  def index
    respond_to do |format|
      format.html
      format.bobo_v1_json { render json: { things: [] } }
    end
  end
end
