Rails.application.routes.draw do
  get '/home', to: 'home#index', as: :home
  resources :things, only: %i[index show create]
end
