Mimenator.register(:bobo_v1_json, tree: :vnd) do |type|
  type.subtype = 'bobo'
  type.version = 1
  type.version_style = :trailing
  type.suffix = :json
end
