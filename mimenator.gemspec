$:.push File.expand_path('../lib', __FILE__)

# Maintain your gem's version:
require 'mimenator/version'

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = 'mimenator'
  s.version     = Mimenator::VERSION
  s.author      = 'Steve Loveless'
  s.email       = 'steve@agrian.com'
  s.homepage    = 'http://bitbucket.com/agrian/mimenator'
  s.summary     = 'Rails plugin for defining vendor/personal/unregistered media types.'
  s.description = s.summary
  s.license     = 'MIT'

  s.files = Dir['{lib}/**/*', 'MIT-LICENSE', 'Rakefile', 'README.md', 'History.md']
  s.test_files = Dir['test/**/*']

  s.add_dependency 'rails', '~> 4.0'
  s.add_development_dependency 'sqlite3'
  s.add_development_dependency 'minitest-reporters'
end
