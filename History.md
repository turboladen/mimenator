### 0.2.0 / 2014-06-26

* Refactored internal use of {{Mimenator.formats}} -> {{Mimenator.types}}.
* Changed dynamic defining of #to_ methods on models to be on
  {{ActiveRecord::Base.inherited}} instead of on the including of
  {{ActiveModel::Conversion}}.  This seems more straightforward to debug, but means
  these methods won't be defined on tableless models.
* Added {{Mimenator::ControllerMixin}} to fix (hack?) setting the respond_to
  format in controller actions.
  
### 0.1.0 / 2014-05-28

* Happy Birthday!
* Allow for defining JSON types.
